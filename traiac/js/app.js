$(function () {
    // Animated scrollTop
    $('#go_top').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    // Navigation drop down
    $('#nav_trigger').click(function () {
        $('.nav_wrap').slideToggle();
    });

    // Animated counter
    $('.count').each(function () {
        var countItem = $(this),
            maxCount = countItem.attr('data-max'),
            animateDuration = parseInt(countItem.attr('data-duration'));
        countItem.prop('Counter', 0).animate({
            Counter: maxCount
        }, {
            duration: animateDuration,
            easing: 'swing',
            step: function (now) {
                countItem.text(Math.ceil(now));
            }
        });
    });
    $('.quick_head').click(function () {
        $('.quick_form').slideToggle();
    });
});