$(function () {
    'use strict';
    $(document).on('click', '.nav_trigger', function () {
        $(this).toggleClass('open');
        if ($(this).find('i').hasClass('ion-navicon')) {
            $(this).find('i').removeClass('ion-navicon').addClass('ion-close');
        } else if ($(this).find('i').hasClass('ion-close')) {
            $(this).find('i').removeClass('ion-close').addClass('ion-navicon');
        }
        $('.responsive_nav').toggleClass('open');
    });
    // Show file upload thumbnail before submitting the form
    function showThumbnail(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            $(input).nextAll('.image_upload_preview').remove();
            reader.onload = function (e) {
                $(input).after('<img class="image_upload_preview" src="' + e.target.result + '" alt=""/>');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on('change', 'input[type="file"][data-action="show_thumbnail"]', function () {
        showThumbnail($(this).get(0));
    });
    
    //date picker 
    $('.datepicker').datepicker({
   	format: 'dd-mm-yyyy'
   });
   // Hiding session message after a few seconds
    setTimeout(function () {
        $('div[data-role="auto-hide"]').fadeOut();
    }, 5000);
    $(document).on('click', 'div[data-role="auto-hide"]', function () {
        $(this).fadeOut();
    });
});