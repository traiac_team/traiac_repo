<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_Model{
 protected $table='login';
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }
    public function resolve_user_login($username, $password) {
        $this->db->select('password');
        $this->db->from('login');
        $this->db->where('userName', $username);       
        $hash = $this->db->get()->row('password');
        $res=$this->verify_password_hash($password, $hash);
        if($res)
        {	
        	$this->db->select('id');
        	$this->db->from('login');
        	$this->db->where('userName', $username);  
        	$id   = $this->db->get()->row('id');
        	return $id;					
		}
		else{
			return 0;
		}
    }
    private function verify_password_hash($password, $hash) {
        return password_verify($password, $hash);
    }
    
    public function get_user($user_id) {

        $this->db->from('login');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();

    }
    public function checkPasswordMatch ($password, $hash) {
        return $this->verify_password_hash($password, $hash);
    }
    
     public function updatePassword ($user_id, $password) {
        $data = ["password" => $this->hash_password($password)];
        $this->db->set($data);
        $this->db->where('id', $user_id);
        if ($this->db->update('login')) {
            return true;
        } else {
            return false;
        }
    }
    
     /**
     * hash_password function.
     *
     * @access private
     * @param mixed $password
     * @return string|bool could be a string on success, or bool false on failure
     */
    private function hash_password($password) {

        return password_hash($password, PASSWORD_BCRYPT);

    }
    
}