<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Questions
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Questions</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Question</th>  
                                <th>Topic</th>
                                <th>Answer</th>
                                <th>Image</th>                               
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($records) > 0) {
                            	//$i =$this->uri->segment(3); 
                            	$i =$this->uri->segment(3);
                                foreach ($records as $data) {
                                	$i++;
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $data->name; ?></td>
                                        <td><?= $data->mobile; ?></td> 
                                        <td><?= $data->email; ?></td>
                                        <td><?= $data->question; ?></td> 
                                        <td><?= $data->topic; ?></td> 
                                        <td><?= $data->answer; ?></td>
                                        <td><img src="<?= base_url(); ?>/uploads/question/<?= $data->image;?>"></td></td>
                                        <td style="width: 150px">
                                <a href="#" data-toggle="modal" data-target="#myModal<?php echo $data->id; ?>" class="btn btn-success btn-flat">Reply</a>
                                <a href="<?= site_url(); ?>/Questions/delete/<?= $data->id; ?>" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a>  
                                            <!-- Modal -->
											<div id="myModal<?php echo $data->id; ?>" class="modal fade" role="dialog">
											  <div class="modal-dialog">

											    <!-- Modal content-->
											   <form action="<?php echo site_url();?>/Questions/addAnswer" method="post" enctype="multipart/form-data">
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Add Reply</h4>
											      </div>
											    
											    <div class="modal-body">
											        <!--<p>Some text in the modal.</p>-->
											        <input type="hidden" name="editId" value="<?php echo $data->id; ?>">
											        <div class="col-lg-4 col-md-4 col-sm-4" style="display: none">    
					                                    <div class="form-group">
					                                        <label for="title">Add date<span class="text-danger">*</span></label> 
					                                        <input type="text" class="form-control datepicker" placeholder="Add Date" name="answerDate" id="answerDate" required="" value="<?php echo date('d-m-Y') ?>">
					                                    </div>    
				                                 	</div> 
											        <div class="row">                               
						                               <div class="col-lg-6 col-md-6 col-sm-6">
						                                    <div class="form-group">
						                                        <label for="topic">Topic<span class="text-danger">*</span></label> 
						                                        <input type="text" class="form-control" placeholder="Topic" name="topic" id="topic" value="<?= $data->topic; ?>">
						                                    </div> 
						                               </div> 
						                            </div>
						                            <div class="row"> 
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
						                                    <div class="form-group">
						                                        <label for="answer">Answer<span class="text-danger">*</span></label> 
						                                        <textarea class="form-control" placeholder="Answer" name="answer" id="answer" required="" rows="10"><?= $data->answer; ?></textarea>
						                                    </div> 
						                               	</div> 
						                            </div>
						                            <div class="row"> 
						                               <div class="col-lg-6 col-md-6 col-sm-6">
						                                    <div class="form-group">
						                                        <label for="title">Image<span class="text-danger">*</span></label> 
						                                        <input type="file" class="form-control" placeholder="Image" name="image" id="image" >
						                                    </div> 
						                               </div>   
											      	</div>
											      </div>
											      <div class="modal-footer">
											        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
											      </div>
											     </div>											      
												</form>
											  </div>
											</div>                                       
                                        </td>
                                    </tr>
                                    <?php                                   
                                }
                            } else {
                                ?>
                                <tr><td colspan="4" align="center">No records found.</td></tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!-- pagination end -->
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>