<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12"><h4>Welcome to the admin panel of Traiac</h4></div>
        </div>
        <?php
/*        var_dump($_SESSION);
        */?>
        <div class="row">
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('banner');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Banner</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-image"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Banner/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('team');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Team</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Team/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('youtube_video');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Youtube Videos</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-video-camera"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Youtube/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('features');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Features</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-newspaper-o"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Features/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('gallery');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Gallery</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-file-image-o"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Gallery/adminIndex" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('questions');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Questions</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Questions/adminIndex" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
