<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Banner
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Banner</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Banner/add" method="post"  enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="title">Title</label><span class="text-danger">*</span>
                                        <input type="text" name="title" id="title" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Caption</label><span class="text-danger">*</span>
                                        <input type="text" name="caption" id="caption" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                    	<label for="colorCode">Caption Colour</label><span class="text-danger">*</span>
                                        <input type="text" name="colorCode" id="colorCode" class="jscolor form-control" required> 
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Banner Image</label><span class="text-danger">*</span>
                                        <input type="file" name="image" id="image" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
