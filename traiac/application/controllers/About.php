<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    public function index()
    {
        $this->load->helper('url');
        $this->load->view('frontend/header');
        $this->load->view('frontend/about');
        $this->load->view('frontend/footer');
    }
}
