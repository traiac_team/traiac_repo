<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	
	 public function __construct() { 
         parent::__construct();       
    	 //$this->load->model(array('Contact_model'));
        //for email function  
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('url');    
      } 
	

    public function index()
    {
        $this->load->helper('url');
        $this->load->view('frontend/header');
        $this->load->view('frontend/contact');
        $this->load->view('frontend/footer');
    }
    
    public function mail()
    {
		$name					= NULL;
     	$email					= NULL;
     	$subject		    	= NULL; 
     	$message		    	= NULL;  
     	 	
     	$submit 				= NULL;
     	
     	extract($_POST);
     	$params['name']	 				=	$name;     
     	$params['email']				=	$email;
     	//$params['phone']	 			=	$phone;   
     	$params['subject']	 			=	$subject; 
     	$params['message']				=	$message;
     	
     	if(isset($submit))
     	{
     		//for mail function
                  $emailTo = 'alisha.bodhi@gmail.com';
			     	$subject = "Traiac Queries";
			     	$msg = "Name:".$name."\n"."Email:".$email."\n"."Your Queries:".$message;
			     	mail($emailTo ,$subject,$msg);
			     	
     	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Your queries send successfully!"]);
     	
     	redirect('Contact/index');	
     	
     	}
     	else {
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to send query!"]);
			redirect('Contact/index');
		}
	}

        public function quick_mail() {
		$name					= NULL;
     	$email					= NULL;
     	$subject		    	= NULL; 
     	$message		    	= NULL;  
     	 	
     	$submit 				= NULL;
     	
     	extract($_POST);
     	$params['name']	 				=	$name;     
     	$params['email']				=	$email;
     	//$params['phone']	 			=	$phone;   
     	$params['subject']	 			=	$subject; 
     	$params['message']				=	$message;
     	
     	if(isset($submit))
     	{
     		//for mail function
                $emailTo = 'alisha.bodhi@gmail.com';
			     	$subject = "Traiac Queries";
			     	$msg = "Name:".$name."\n"."Email:".$email."\n"."Your Queries:".$message;
			     	mail($emailTo,$subject,$msg);
			     	
     	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Your queries send successfully!"]);
     	
     	redirect($_SERVER['HTTP_REFERER']);	
     	
     	}
	}



}
