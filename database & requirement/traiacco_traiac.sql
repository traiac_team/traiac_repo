-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 29, 2017 at 06:12 AM
-- Server version: 10.0.30-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `traiacco_traiac`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(200) NOT NULL,
  `caption` text NOT NULL,
  `colorCode` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `image`, `title`, `caption`, `colorCode`) VALUES
(19, '2ee76c7cb407d7c21ac1cd9eab3ef702.jpg', 'TRAIAC AUTOMATION', '50 %  Inauguration Offer ', 'FCFFFD'),
(20, '77a896e99b2ad955bf3e5075541faedd.jpg', 'TRAIAC AUTOMATION', '50 %  Inauguration Offer ', 'FFFFFF'),
(21, 'd6e3f12c6308bbbfbce33b862f03db1b.jpg', 'TRAIAC AUTOMATION', '50 %  Inauguration Offer ', 'FFFFFF'),
(22, '36ad8969146e43e034dc851a917988f7.jpg', 'TRAIAC AUTOMATION', '50 %  Inauguration Offer ', 'FFFFFF'),
(23, '6eac69985f35995a6b15b727c6c19de8.jpg', 'TRAIAC AUTOMATION', '50 %  Inauguration Offer ', 'FFFFFF'),
(24, 'f35fcc1d77466be573b008340a084c31.jpg', 'TRAIAC AUTOMATION', '50 %  Inauguration Offer ', 'FFFFFF');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `title`, `description`, `image`) VALUES
(1, 'Calibration & Quality control', 'We are too the beneficiaries of calibration . Hence we have to know what is calibration and its methodologies.  Quality should be also a guaranteed thing. So briefing of quality manuals and standards are quite important. ', '56653d38195b0df9241c56cf296cd354.png'),
(2, 'Centralized SCADA Console', 'First time in kerala we are introduced a full fledged SCADA central Console station with HD led Screens, which helps in online monitoring of different parameter variations and plant simulation . We are also providing LABVIEW', 'e4d129124bf54c6c6fbfd16972ddde68.png'),
(4, 'Pneumatic Instrumentation', 'Pneumatic instrumentation is an unavoidable part of  offshore instrumentation. because of its versatility. So  we have a 10 bar pneumatic test facility with various pneumatic sensors and actuators.  Various pneumatic logic\'s are demonstrated. ', '6f1d3d7e5f44253f3f393ea93dd04818.png'),
(5, 'Advanced PLC Lab', 'We have a automation lab with more than seven plc\'s , various industrial  demonstrations are interfaced with these Plc\'s. And we have a dedicated control panels for each plc which give a provision for controlling the basic inputs and external feeds', 'd791af29a85d4ddacac55e2b0d633f43.png');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `caption` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `image`, `caption`) VALUES
(9, '721d5cea2d08271cb95352b512a48d4c.jpg', 'Broucher'),
(10, 'f18bfcbe30c21a02465d3da34c711e0c.jpg', 'Broucher'),
(11, '401e150ff607e3429ecbe833fc6cd266.jpg', 'a'),
(12, 'eab4dba3abb306e30ca1628042e30941.jpg', '3'),
(13, 'dd852d06aeab0b870533c69269c65320.jpg', '2'),
(14, '255f1d61e67382e036201045ba7decf1.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `latest_update`
--

CREATE TABLE `latest_update` (
  `id` int(11) NOT NULL,
  `updates` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `latest_update`
--

INSERT INTO `latest_update` (`id`, `updates`) VALUES
(6, 'Traiac\'s greetings to All.....Hurry...!!!! Join South India\'s Biggest Automation LAB....Inauguration offer 50 %.. Call us :  7012776582      Join us.... <<<< Required: Automation Trainees, Technical Sales Engineers, HR ..... Send your bio data to hr@traiac.com  >>>>> ');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `userName`, `password`) VALUES
(1, 'admin', '$2y$10$89lupcFDZJoZPz7iPuI8xOAxhMxM8cPb8gDD8VUUUZDzZlf6ndvbO');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `question` text NOT NULL,
  `questionDate` date NOT NULL,
  `topic` text NOT NULL,
  `answer` text NOT NULL,
  `image` text NOT NULL,
  `answerDate` date NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `name`, `email`, `mobile`, `question`, `questionDate`, `topic`, `answer`, `image`, `answerDate`, `status`) VALUES
(8, 'Rhoel G Junio', 'arun@gmail.com', '', 'Rakhil kp. Calicut', '2017-03-11', 'what is meant by 4 - 20 ma current  output?', 'Sensors or other devices with a 4-20 mA current loop output are extremely common in industrial measurement and control applications. They are easy to deploy, have wide power supply requirements, generate a low noise output, and can be transmitted without loss over great distances. We encounter them all the time in both process control and basic measurement data logger and data acquisition applications.', 'b3f53e12994e22325f1350b982e83da9.png', '2017-03-11', 'replied'),
(9, 'Akhil k Ramesh', 'arun@gmail.com', '', 'Anas P.  Kanoor', '2017-03-11', 'What is the difference between HART And SMART Transmitters?', 'HART = Highway Addressable Remote Transducer-. is a communication protocol.\r\nSMART = Single Modular Auto-ranging Remote Transducer.\r\nSMART instruments can be HART,FF or Profibus.\r\nSMART transmitters compared to analog transmitters have microprocessor as their integral part , which helps for self diagnostic abilities , non-linear compensations , re-ranging without performing calibrations ,and ability to communicate digitally over the network.\r\nHART standard helps instruments to digitally communicate with one another over the same two wires used to convey a 4-20 ma analog instrument signal.', '1e974ba9d09a579dd80e6aedd1397f74.png', '2017-03-11', 'replied'),
(10, 'Akhil k Ramesh', 'arun@gmail.com', '', 'Mahesh Kumar. Pune', '2017-03-11', 'what is hydrotest?', 'A hydrostatic test is a way in which pressure vessels such as pipelines, plumbing, gas cylinders, boilers and fuel tanks can be tested for strength and leaks.', 'e2d0083a02cde6152b6df304b4d7d805.png', '2017-03-11', 'replied'),
(11, 'Akhil k Ramesh', 'arun@gmail.com', '', 'Justin Jacob. Mangalore', '2017-03-11', 'H2S Safety?', 'Hydrogen Sulfide (H2S) Hydrogen sulfide is a colorless, flammable, extremely hazardous gas with a “rotten egg” smell. It occurs naturally in crude petroleum and natural gas, and can be produced by the breakdown of organic matter and human/ animal wastes (e.g., sewage).\r\n', '0b78e7cae5b3805fafab95e66cd26ef8.png', '2017-03-11', 'replied'),
(12, 'Akhil k Ramesh', 'arun@gmail.com', '', 'Sherin S. Hariyana', '2017-03-11', 'what is the use of chart recorder?', 'A chart recorder is an electro mechanical device that records an electrical or mechanical input trend onto a piece of paper (the chart). Chart recorders may record several inputs using different color pens and may record onto strip charts or circular charts.\r\n', 'ac69981b84ff7801a20c5454b7a5238c.png', '2017-03-11', 'replied'),
(20, 'e', 'e@e.com', '', 'u', '2017-05-27', '', '', '', '0000-00-00', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `description`, `image`) VALUES
(1, 'Rhoel G Junio', 'Quality engineer from Philippines. He has more than 25 years experience in linear instrumentation & production ', '976c137ed76b4845145e38ab38ee8d63.png'),
(2, 'Akhil K Ramesh', 'Project Associate  at  IIT Delhi, Former Assistant Professor  at College of Engineering Munnar ', 'c01cf5a9744bb6c20dd13b9846cf356b.png');

-- --------------------------------------------------------

--
-- Table structure for table `youtube_video`
--

CREATE TABLE `youtube_video` (
  `id` int(11) NOT NULL,
  `videoLink` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `youtube_video`
--

INSERT INTO `youtube_video` (`id`, `videoLink`) VALUES
(2, 'km8MSWm39Z0'),
(3, 'IyzZ4EHTsRU'),
(5, 'wC4vITSVXoA'),
(7, 'm8n2o3Qx504');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `latest_update`
--
ALTER TABLE `latest_update`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `youtube_video`
--
ALTER TABLE `youtube_video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `latest_update`
--
ALTER TABLE `latest_update`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `youtube_video`
--
ALTER TABLE `youtube_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
