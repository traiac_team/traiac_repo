<?php
 class Gallery_model extends CI_Model {
	protected $table='gallery';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData($limit,$offset){
	    $this->db->select('*');
		$this->db->from('gallery');
		$this->db->order_by('gallery.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function insertData($params){ 
		$ins		  =	$this->db->insert($this->table,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	public function getUpdateData($params)
	{ 
		$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table,$params['condition']);
		return $query->result_array();		
	}
	public function rowWiseData($editId)
	{
		$query 	= $this->db->query("SELECT image FROM gallery where id='$editId'");
		$row 	= $query->row();
		//echo $this->db->last_query();die;
		return $row;
	} 
	public function updateAction($params,$editId)
	{
		$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
    }

     //taking all data from gallery for showing into front end
    public function getAllDataFront() {
		$this->db->select('*');
		$this->db->from('gallery');
		$this->db->order_by('gallery.id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
 

}