    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Powered By</b> <a href="http://bodhiinfo.com" target="_blank"><img src="<?= base_url(); ?>images/bodhi_logo.png" alt="Bodhi"></a>
        </div>
        <strong>Copyright &copy; <script>document.write(new Date().getFullYear());</script> <a href="<?= base_url(); ?>">Traiac</a>.</strong> All rights
        reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.4 -->
<script src="<?= base_url(); ?>js/jquery-2.2.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url(); ?>js/bootstrap.min.js"></script>
<!-- datepicker -->
<script src="<?= base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Slimscroll -->
<script src="<?= base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url(); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>js/app.min.js"></script>
<script src="<?= base_url(); ?>js/custom.js"></script>
<script src="<?= base_url(); ?>js/jscolor.js"></script>
<script src="<?= base_url(); ?>js/jscolor.min.js"></script>
</body>
</html>
