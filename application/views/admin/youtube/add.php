<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            YouTube Video
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New YouTube Video</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10">
                                <form action="<?= site_url(); ?>/Youtube/add" method="post">
                                    <div class="form-group">
                                        <label for="videoLink">Video Link</label><span class="text-danger">*</span>
                                        <input type="text" name="videoLink" id="videoLink" class="form-control" required>
                                    </div>                                    
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
