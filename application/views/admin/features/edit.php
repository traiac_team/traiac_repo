<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit Features
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Features</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Features/edit" method="post"  enctype="multipart/form-data">
                                 <?php foreach($results as $r){  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                        <label for="title">Title</label><span class="text-danger">*</span>
                                        <input type="text" name="title" id="title" class="form-control" required value="<?= $r['title']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Description</label><span class="text-danger">*</span>
                                        <input type="text" name="description" id="description" class="form-control" required value="<?= $r['description']; ?>">
                                    </div>
                                    <!--<div class="form-group">
                                        <label for="title">Image</label><span class="text-danger">*</span>
                                        <input type="file" name="image" id="image" class="form-control" required>
                                    </div>-->
                                    <div class="form-group">
                                        <label for="description">Image want to change..?</label><span class="text-danger">*</span>
                                       <input type="radio" name="changeImage" id="ImageChange"  value="yes" onclick="imgDisply(this.value)">Y
                                       <input type="radio" name="changeImage" id="ImageNoChange"  value="no" onclick="imgDisply(this.value)">N
                                    </div>
                                    <div class="form-group" style="display: none" id="featureImage">
                                        <label for="property_image">Feature Image (768x576)</label>
                                        <input type="file" name="image" data-action="show_thumbnail">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>
function imgDisply(radioValue)
{
	if(radioValue=='yes')
	{
		document.getElementById('featureImage').style.display='block';
	}
	if(radioValue=='no')
	{
		document.getElementById('featureImage').style.display='none';
	}
}
</script>
