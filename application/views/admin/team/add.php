<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Team
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Banner</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Team/add" method="post"  enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="name">Name</label><span class="text-danger">*</span>
                                        <input type="text" name="name" id="name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label><span class="text-danger">*</span>
                                        <input type="text" name="description" id="description" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Team Image</label><span class="text-danger">*</span>
                                        <input type="file" name="image" id="image" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
