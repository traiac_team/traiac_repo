<div class="container pad_50_15">
   <?php
    if ($this->session->flashdata('flash')) {
        ?>
        <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
            <?= $this->session->flashdata('flash')['message']; ?>
        </div>
        <?php
    }
    ?>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <h3 class="contact_left_head" style="margin-top: 0">Your Ignorance is Our Knowledge</h3>
            <p class="contact_left_desc">Ask your doubts and get Satisfactory answers..!</p>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
           
            <form class="contact_form" action="<?php echo site_url(); ?>/Questions/add" method="post">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="form-group">
                            <input type="text" name="name" placeholder="Your Name" required="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="form-group">
                            <input type="email" name="email" placeholder="Your Email" required="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" name="mobile" placeholder="Mobile">
                </div>
                <div class="form-group">
                    <textarea name="question" placeholder="Your Question" required=""></textarea>
                </div>
                <div id="recaptcha2"></div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Submit Question">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="placement_questions">
    <div class="container">
        <?php
        foreach($records as $question) {
            ?>
            <div class="question">
                <div class="question_image"><?php if($question -> image=="") { ?><img src="<?= base_url(); ?>uploads/question/1.png" alt=""><?php } else { ?><img src="<?= base_url(); ?>/uploads/question/<?= $question -> image ?>" alt=""><?php } ?></div>
                <div class="question_det">
                    <h3><?= $question -> topic; ?></h3>
                    <h4><?= $question -> question; ?></h4>
                    <div class="answer"><?= $question -> answer; ?></div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<div class="testimonial_wrap">
    <div class="testimonial_inner pad_50">
        <div class="container">
            <h2 class="testimonial_head text-center text-uppercase">Success Stories</h2>
            <div class="testimonial">
                <ul>
                    <li>
                        <div class="testimonial_block">
                            <div class="testimonial_pic">
                                <img src="<?= base_url(); ?>images/anupam_koova.jpg" alt="">
                            </div>
                            <h4>Anupam Koova</h4>
                            <p>Hi,<br />After completing my bachelor degree in Electronics and instrumentation, I had taken a training in PLC and SCADA which helped me getting a job abroad in the field of automation, calibration and off shore, and my life is secure now.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="testimonial_block">
                            <div class="testimonial_pic">
                                <img src="<?= base_url(); ?>images/annie_jacob_varghese.jpg" alt="">
                            </div>
                            <h4>Annie Jacob Varghese.</h4>
                            <p>Hello friends,<br/>Automation training in PLC and SCADA helped me a lot in my professional life. Earlier after my graduation i was working in a small scale industry and undergone automation training. After that i got a new job as Automation and Design Engineer. So i recommend my young friends to also take this skill developmemt training. </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
