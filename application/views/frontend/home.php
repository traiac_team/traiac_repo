<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Traiac Automation</title>
    <meta name="keywords" content="automation training in kerala, best automation institute in kerala, top automation center in kerala, instrumentation companies in kerala, automation companies in kerala, process controll instrumentation, indias best automation institute, instrumentation jobs in kerala, automation jobs in kerala, automation companies in kochi, automation training in kochi, plc training kochi, plc training kerala">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/lightgallery.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css">
</head>
<body>
    <div class="top">
        <div class="container clearfix">
            <div class="top_left">
                <span class="top_left_items">
                    <strong>Dail:</strong>
                </span>
                <span class="top_left_items">
                    <img src="<?= base_url(); ?>images/call.png" alt="">
                    <a href="tel:+917012776582">+917012776582</a>
                </span>
                <span class="top_left_items">
                    <strong>Mail To:</strong>
                </span>
                <span class="top_left_items">
                    <img src="<?= base_url(); ?>images/mail.png" alt="">
                    <a href="mailto:info@traiac.com">info@traiac.com</a>
                </span>
            </div>
            <div class="top_right">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
            </div>
        </div>
    </div>
    <header class="common">
        <div class="container clearfix">
            <div class="logo">
                <a href="<?= base_url(); ?>index.php/Home/index"><img src="<?= base_url(); ?>images/logo.png" alt="Traiac" /></a>
            </div>
            <div class="nav_wrap">
                <ul class="clearfix">
                    <li><a href="<?php echo site_url();?>/Home">Home</a></li>
                    <li><a href="<?php echo site_url();?>/About">About Us</a></li>
                    <li><a href="<?php echo site_url();?>/Service">Services</a></li>
                    <li><a href="<?php echo site_url();?>/Questions">Students Portal</a></li>
                    <li><a href="<?php echo site_url();?>/Gallery">Gallery</a></li>
                    <li><a href="<?php echo site_url();?>/Contact">Contact Us</a></li>
                </ul>
            </div>
        </div>
        <span id="nav_trigger"><i class="fa fa-bars"></i></span>
    </header>
    <div class="banner_wrap">
        <div class="res_banner_wrapper">
            <div class="banner_control_wrap">
                <div class="container">
                    <div class="banner_control clearfix">
                        <div class="banner_control_nav" id="banner_prev"></div>
                        <div class="banner_control_nav" id="banner_next"></div>
                    </div>
                </div>
            </div>
            <ul class="banner">
                <?php 
            foreach($banners as $b)
            {
			?>
            <li>
                <div class="banner_box">
                    <img src="<?= base_url(); ?>uploads/banner/<?php echo $b->image; ?>" alt="" />
                    <div class="cap_wrap">
                        <div class="cap_block" style="color: <?= '#'.$b->colorCode; ?>">
                            <h2 class="text-capitalize"><?= $b->title ?></h2>
                            <p><?= $b->caption ?></p>
                        </div>
                    </div>
                </div>
            </li>
			<?php
			}
            ?>
            </ul>
        </div>
    </div>
    <div class="news_ticker">
        <div class="news_ticker_head"><h4><strong>Latest Updates</strong></h4></div>
        <div class="news_ticker_inner_wrapper">
            <div class="news_ticker_inner">
            	<?php foreach($update as $u) { ?>
                <a href="javascript:void(0)"><?= $u->updates; ?>.</a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="sec welcome">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h1 class="welcome_head">Traiac</h1>
                    <p>The word 'Automation' is so familiar nowadays..It is echoed in every field, as it makes our job more easy and technologically improved.<br />TRAIAC  Automation Research makes you ready and adaptable for this scenario. We meet every instrumentation and Automation needs of any industry. By introducing one of the best Practical and Research lab in India we aim to mould young engineers more technologically oriented and practically prominent innovators, who materialise the dreams of the customers..</p>
                    <div class="team">
                        <h4 class="text-right"><b>We will guide you...</b></h4>
                        
                        <div class="team_slider">
                            <ul>
                            <?php foreach($records as $r) { ?>
                                <li>
                                    <div class="team_img"><img class="img-circle" src="<?= base_url(); ?>uploads/team/<?php echo $r->image ?>" alt="" /></div>
                                    <div class="team_det">
                                        <h4 style="color: #287dc6"><?php echo $r->name ?></h4>
                                        <p><?php echo $r->description ?></p>
                                    </div>
                                </li>
                              <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="video_slider_wrapper">
                        <ul class="video_slider">
                            <?php 
			            foreach($videos as $v)
			            {
						?>
                            <li data-ind="0">
                                <div class="video_wrap embed-responsive embed-responsive-16by9">
                                    <iframe src="https://www.youtube.com/embed/<?= $v->videoLink ?>?enablejsapi=true&version=3"></iframe>
                                </div>
                            </li>
                        <?php
                        }
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hm_courses_wrap">
        <div class="container">
            <div class="hm_course_row">
            	<?php $prodRow	=	0;
            	foreach($features as $f) {
					
            	 ?>
                <div class="hm_course_item clearfix">
                    <div class="ico"><img class="img-responsive" src="<?= base_url() ?>uploads/features/<?= $f->image; ?>" ></div>
                    <div class="hm_course_det">
                        <h3><?= $f->title ?></h3>
                        <p><?= $f->description ?></p>
                    </div>
                </div>
                <?php 
                
                $prodRow +=1;
                if ($prodRow == 2)
				{ ?>
				</div>
				<?php } } ?>
        </div>
    </div>

    <div class="container gallery_wrap">
        <div class="gallery_hm_row clearfix">
            <div class="gallery_hm_column clearfix">
                <div class="gallery_hm_column">
                    <a class="hm_gal_lnk text-center" href="<?php echo site_url();?>/Gallery"><span class="main text-left">Our<br /><span>Gallery</span></span></a>
                </div>
                <div class="gallery_hm_column">
                    <a href="<?= base_url() ?>uploads/gallery/<?= $gallery[0]['image'] ?>" class="gal_item" style="background-image: url('<?= base_url() ?>uploads/gallery/<?= $gallery[0]["image"] ?>')">
                        <div class="overlay"></div>
                    </a>
                </div>
            </div>
            <div class="gallery_hm_column">
                <a href="<?= base_url() ?>uploads/gallery/<?= $gallery[1]['image'] ?>" class="gal_item" style="background-image: url('<?= base_url() ?>uploads/gallery/<?= $gallery[1]["image"] ?>')">
                    <div class="overlay"></div>
                </a>
            </div>
        </div>
        <div class="gallery_hm_row clearfix">
            <div class="gallery_hm_column">
                <a href="<?= base_url() ?>uploads/gallery/<?= $gallery[2]['image'] ?>" class="gal_item" style="background-image: url('<?= base_url() ?>uploads/gallery/<?= $gallery[2]["image"] ?>')">
                    <div class="overlay"></div>
                </a>
            </div>
            <div class="gallery_hm_column clearfix">
                <div class="gallery_hm_column">
                    <a href="<?= base_url() ?>uploads/gallery/<?= $gallery[3]['image'] ?>" class="gal_item" style="background-image: url('<?= base_url() ?>uploads/gallery/<?= $gallery[3]["image"] ?>')">
                        <div class="overlay"></div>
                    </a>
                </div>
                <div class="gallery_hm_column">
                    <a href="<?= base_url() ?>uploads/gallery/<?= $gallery[4]['image'] ?>" class="gal_item" style="background-image: url('<?= base_url() ?>uploads/gallery/<?= $gallery[4]["image"] ?>')">
                        <div class="overlay"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php
    if (count($questions) > 0) {
        ?>
        <div class="hm_question_wrapper">
            <div class="hm_question_inner">
                <div class="container">
                    <h2 class="hm_question_head">Your Questions Answered</h2>
                    <ul class="question_slider">
                        <?php
                        foreach ($questions as $question) {
                            ?>
                            <li>
                                <a href="<?= site_url();?>/Questions" class="question_slider_item">
                                    <div class="question_slider_img">
                                        <img src="<?= base_url(); ?>/uploads/question/<?= $question -> image ?>" alt="" />
                                    </div>
                                    <h3><?= $question -> topic; ?></h3>
                                    <h4><?= $question -> question; ?></h4>
                                    <div class="question_slider_answer"><?= $question -> answer; ?></div>
                                </a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="client_wrap">
        <div class="container text-center">
            <img src="<?= base_url(); ?>images/1.jpg" alt="">
            <img src="<?= base_url(); ?>images/2.png" alt="">
            <img src="<?= base_url(); ?>images/3.png" alt="">
            <img src="<?= base_url(); ?>images/4.png" alt="">
            <img src="<?= base_url(); ?>images/5.png" alt="">
            <img src="<?= base_url(); ?>images/6.png" alt="">
            <img src="<?= base_url(); ?>images/7.jpg" alt="">
            <img src="<?= base_url(); ?>images/8.jpg" alt="">
        </div>
    </div>

    <footer>
        <div class="container text-center">
            <div class="footer_top">
                <a href="#" id="go_top">
                    <div><i class="fa fa-caret-up"></i></div>
                    <div>TOP</div>
                </a>
                <div class="footer_top_bg"></div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div id="home_map">

                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9">
                    <div class="footer_logo">
                        <a href="index.html"><img src="<?= base_url(); ?>images/logo.png" alt="Traiac"></a>
                    </div>
                    <div class="brochure_lnk">
                        <a href="#">Download Our Brochure</a>
                    </div>
                    <div class="footer_address">
                        <img src="<?= base_url(); ?>images/map_marker.png" alt="">
                        Savera Tower, Opp MV Hotel, Thrissur - Calicut Highway, Changaramkulam
                    </div>
                    <div class="footer_contact">
                        <span>
                            <img src="<?= base_url(); ?>images/call_white.png" alt="">
                            <a href="tel:+917012776582">+91 7012 776 582</a>
                        </span>
                        <span>
                            <img src="<?= base_url(); ?>images/mail_white.png" alt="">
                            <a href="mailto:info@traiac.com">info@traiac.com</a>
                        </span>
                    </div>
                    <div class="footer_social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="copy_wrap clearfix">
                <div class="copy">
                    <script>document.write(new Date().getFullYear());</script>
                    Copyright &copy; Traiac. All rights reserved.
                </div>
                <div class="power">
                    Powered by <a href="http://bodhiinfo.com" target="_blank"><img src="<?= base_url(); ?>images/bodhi.png" alt="Bodhi"></a>
                </div>
            </div>
        </div>
    </footer>



    <div class="quick_contact">
        <div class="quick_head">Quick Contact</div>
        <div class="quick_form">
            <script src='https://www.google.com/recaptcha/api.js'></script>  
            <form action="<?php echo site_url(); ?>/Contact/quick_mail" method="post">
                <div class="form-group">
                    <input type="text" name="name" placeholder="Your Name" required>
                </div>
                <div class="form-group">
                    <input type="email" name="email" placeholder="Your Email" required>
                </div>
                <div class="form-group">
                    <input type="text" name="subject" placeholder="Subject" required>
                </div>
                <div class="form-group">
                    <textarea name="message" placeholder="Your Message"></textarea>
                </div>
                <div class="g-recaptcha" data-sitekey="6Ld1DCMUAAAAAIaMtYfQTbOCEhYC31caw9_ZLyEn"></div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Submit Message">
                </div>
            </form>
        </div>
    </div>
    <div class="image_preload"></div>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="<?= base_url(); ?>js/jquery.fitvids.js"></script>
    <script src="<?= base_url(); ?>js/jquery.bxslider.min.js"></script>
    <script src="<?= base_url(); ?>js/lightgallery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    <script src="<?= base_url(); ?>js/lg-fullscreen.min.js"></script>
    <script src="<?= base_url(); ?>js/lg-thumbnail.min.js"></script>
    <script src="<?= base_url(); ?>js/jquery.marquee.min.js"></script>
    <script src="<?= base_url(); ?>js/app.js"></script>
<script src="<?= base_url(); ?>js/jscolor.js"></script>
<script src="<?= base_url(); ?>js/jscolor.min.js"></script>
    <script>
        $(function () {
            var anims = ["fadeIn", "fadeInDown", "fadeInDownBig", "fadeInLeft", "fadeInLeftBig", "fadeInRight", "fadeInRightBig", "fadeInUp", "fadeInUpBig"],
                anim;
            $('.banner').bxSlider({
                pager: false,
                mode: 'horizontal',
                auto: true,
                preloadImages: 'all',
                pause: 5000,
                speed: 1000,
                touchEnabled: false,
                easing: 'ease-in-out',
                nextSelector: '#banner_next',
                prevSelector: '#banner_prev',
                nextText: '<i class="fa fa-caret-right"></i>',
                prevText: '<i class="fa fa-caret-left"></i>',
                onSliderLoad: function () {
                    anim = anims[Math.floor(Math.random() * anims.length)];
                    $('.banner>li .cap_block').eq(1).addClass('active-slide');
                    $(".cap_block.active-slide").addClass("wow animated " + anim);
                },
                onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
                    $('.active-slide').removeClass('active-slide');
                    $('.banner>li .cap_block').eq(currentSlideHtmlObject + 1).addClass('active-slide');
                    anim = anims[Math.floor(Math.random() * anims.length)];
                    $(".cap_block.active-slide").addClass("wow animated " + anim);

                },
                onSlideBefore: function () {
                    $(".cap_block.active-slide").removeClass("wow animated " + anim);
                    $(".one.cap_block.active-slide").removeAttr('style');
                }
            });
            $('.video_slider').bxSlider({
                pager: false,
                useCSS: false,
                video: true,
                onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
                    $('.video_slider iframe').each(function () {
                        $(this).get(0).contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}','*');
                    });
                }
            });

            // Marquee
            $('.news_ticker_inner').marquee({
                pauseOnHover: true,
                duration: 15000,
                allowCss3Support: true,
                css3easing: 'linear',
                gap: 20
            });

            // Team
            $('.team_slider ul').bxSlider({
                auto: true,
                pager: false,
                controls: false
            });

            // Question slider
            if ($('.question_slider').length > 0) {
                $('.question_slider').bxSlider({
                    auto: true,
                    controls: false,
                    adaptiveHeight: true
                });
            }
        });
        jQuery(document).ready(function ($) {
            $('.gallery_wrap').lightGallery({
                download: false,
                thumbnail: false,
                fullScreen: false,
                selector: 'a.gal_item'
            });
        });
    </script>
    <script>
        function initMap() {
            // Styles a map in night mode.
            var myLatLng = {lat: 10.734699, lng: 76.030724},
                contentString = '<div id="content" style="color: #333;"><h4>Traiac Automation</h4><p>Savera Tower, Opp MV Hotel<br/>Thrissur - Calicut Highway<br />Changaramkulam</p></div>';
            var infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 250
            });
            var map = new google.maps.Map(document.getElementById('home_map'), {
                center: myLatLng,
                zoom: 13,
                scrollwheel: false
            });
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Bodhi Info Solutions Pvt Ltd',
                animation: google.maps.Animation.BOUNCE
            });
            /*marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
            map.addListener('idle', function () {
                infowindow.open(map, marker);
            });*/
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj335zTM9brgk0tmx4Fl5CGTeLItgIZbI&callback=initMap" sync defer></script>
</body>
</html>