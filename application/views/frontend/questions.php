
<div class="container pad_50_15">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <h3 class="contact_left_head">Reach Us</h3>
            <p class="contact_left_desc">
                Collaboratively administrate turnkey channels whereas virtual e-tailers. Objectively seize scalable metrics whereas proactive e-services.
            </p>
            <div class="contact_item_wrapper">
                <div class="contact_item">
                    <div class="contact_item_ico"><i class="fa fa-phone"></i></div>
                    <div class="contact_item_det">
                        <p>Phone:</p>
                        <p><a href="tel:1234567891">+91 1234567891</a></p>
                    </div>
                </div>
                <div class="contact_item">
                    <div class="contact_item_ico"><i class="fa fa-fax"></i></div>
                    <div class="contact_item_det">
                        <p>Fax:</p>
                        <p>1234567891</p>
                    </div>
                </div>
                <div class="contact_item">
                    <div class="contact_item_ico"><i class="fa fa-envelope"></i></div>
                    <div class="contact_item_det">
                        <p>Email:</p>
                        <p>1234567891</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <h3 class="contact_left_head">Ask your questions here..</h3>
            
            
            <?php
			        if ($this->session->flashdata('flash')) {
			            ?>
			            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
			                <?= $this->session->flashdata('flash')['message']; ?>
			            </div>
			            <?php
			        }
			        ?>
            <script src='https://www.google.com/recaptcha/api.js'></script>  
            <form class="contact_form" action="<?php echo site_url(); ?>/Questions/add" method="post">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="form-group">
                            <input type="text" name="name" placeholder="Your Name" required="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="form-group">
                            <input type="email" name="email" placeholder="Your Email" required="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" name="mobile" placeholder="Mobile">
                </div>
                <div class="form-group">
                    <textarea name="question" placeholder="Your Question" required=""></textarea>
                </div>
                <div class="g-recaptcha" data-sitekey="6Ld1DCMUAAAAAIaMtYfQTbOCEhYC31caw9_ZLyEn"></div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Submit Question">
                </div>
            </form>
            </div>
        
    </div>
</div>

<script>
    function initMap() {
        // Styles a map in night mode.
        var myLatLng = {lat: 21.485265, lng: 39.193167},
            contentString = '<div id="content"><h4>Traiac</h4></div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 250
        });
        var map = new google.maps.Map(document.getElementById('gmap'), {
            center: myLatLng,
            zoom: 18,
            scrollwheel: false
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Bodhi Info Solutions Pvt Ltd',
            animation: google.maps.Animation.BOUNCE
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
        map.addListener('idle', function () {
            infowindow.open(map, marker);
        });
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj335zTM9brgk0tmx4Fl5CGTeLItgIZbI&callback=initMap" sync defer></script>