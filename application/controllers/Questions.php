<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends CI_Controller {
	protected $baseFolder		=	'admin/question';
	protected $table			=	'questions';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct() { 
         parent::__construct();       
    	 $this->load->model(array('Questions_model'));
         //for email function  
         $this->load->library('session');
         $this->load->library('email');
         $this->load->helper('url');   

        /*if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        } */
      } 
	

    public function index()
    {
        $data['records'] = $this->Questions_model->getFrontendData();
        $this->load->view('frontend/header');
        $this->load->view('frontend/placement', $data);
        $this->load->view('frontend/footer');
    }
     public function adminIndex()
    {
    	$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Questions/adminIndex';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
    	
    	
    	$data['records'] = $this->Questions_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
    }
    
     public function post_captcha($user_response) {
        $fields_string = '';
        $fields = array(
            'secret' => '6Ld1DCMUAAAAAMvNMD6uww1zAO5V7vwGul3W3yx6',
            'response' => $user_response
        );
        foreach($fields as $key=>$value)
        $fields_string .= $key . '=' . $value . '&';
        $fields_string = rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }
	
    public function add()
    {
         // Call the function post_captcha
            $res = $this->post_captcha($_POST['g-recaptcha-response']);
        
            if (!$res['success']) {
                // What happens when the CAPTCHA wasn't checked
           //     echo '<p>Make sure you check the security CAPTCHA box.</p><br>';
                 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Make sure you check the security CAPTCHA box."]);
                		redirect('Questions/index');	
            } else {
                // If CAPTCHA is successfully completed...
            		$name					= NULL;
                 	$email					= NULL;
                 	$mobile			    	= NULL; 
                 	$question		    	= NULL;  
                 	 	
                 	$date					=	date('Y-m-d');
                 	extract($_POST);
                 	$params['name']	 				=	$name;     
                 	$params['email']				=	$email;
                 	$params['mobile']	 			=	$mobile;   
                 	$params['question']	 			=	$question; 
                 	$params['status']				=	'pending';
                 	$params['questionDate']			=	$date;
                 	
                 	if(isset($submit))
                 	{
                 		$res=$this->Questions_model->insertData($params);			
            			 if($res)
            	         {
            	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Question sent successfully!"]);
            			 }
            			 else{
            			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
            			 }
            		}
            		 redirect('Questions/index');
            } 
	}
	 public function addAnswer()
	 {
	 	$topic			= NULL;
     	$answer			= NULL;     	
     	$submit 		= NULL;
     	$editId			= NULL;
     	$date					=	date('Y-m-d');
     	extract($_POST);
     	$params['topic']	=	$topic;
     	$params['answer']	=	$answer;  
     	$params['status']				=	'replied';   
     	$params['answerDate']			=	$date;
     	
     	//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $up_image=array(
            	'upload_path'=>'./uploads/question/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
          $this->upload->initialize($up_image);
          if($this->upload->do_upload('image')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	$params['image']  	=	$fileUpload['file_name'];
		  } 
     	 // echo $fileUpload['file_name'];die; 
     	 
     	if(isset($submit))
     	{		
			$res=$this->Questions_model->updateAction($params,$editId);			
			 if($res)
	         {
	         	/*if($res2) {
					//for mail function
			     	$subject = "Sahara Branch Login Credentials";
			     	$msg = "UserName:".$userName."\n"."Password:".$password;
			     	mail($email,$subject,$msg);
				}
	         	*/
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('Questions/adminIndex');		
	 }
public function getFrontData()
	 { 
	  $data['records'] = $this->Questions_model->getFrontendData(); var_dump($data) ; 
        $this->load->view('frontend/header');
        $this->load->view('frontend/questions');
        $this->load->view('frontend/footer');
    }

    public function delete() { 
         $id = $this->uri->segment('3');
         $res=$this->Questions_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Questions/adminIndex');  		
      }

}
