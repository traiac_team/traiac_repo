<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	
	 public function __construct() { 
         parent::__construct();       
    	 //$this->load->model(array('Contact_model'));
        //for email function  
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('url');    
      } 
	

    public function index()
    {
        $this->load->helper('url');
        $this->load->view('frontend/header');
        $this->load->view('frontend/contact');
        $this->load->view('frontend/footer');
    }
    
    
     public function post_captcha($user_response) {
        $fields_string = '';
        $fields = array(
            'secret' => '6Ld1DCMUAAAAAMvNMD6uww1zAO5V7vwGul3W3yx6',
            'response' => $user_response
        );
        foreach($fields as $key=>$value)
        $fields_string .= $key . '=' . $value . '&';
        $fields_string = rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }
    
    
    public function mail()
    {
        

            // Call the function post_captcha
            $res = $this->post_captcha($_POST['g-recaptcha-response']);
        
            if (!$res['success']) {
                // What happens when the CAPTCHA wasn't checked
           //     echo '<p>Make sure you check the security CAPTCHA box.</p><br>';
                 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Make sure you check the security CAPTCHA box."]);
                			redirect('Contact/index');
            } else {
                // If CAPTCHA is successfully completed...
                    	$name					= NULL;
                     	$email					= NULL;
                     	$subject		    	= NULL; 
                     	$message		    	= NULL;  
                     	 	
                     	$submit 				= NULL;
                     	
                     	extract($_POST);
                     	$params['name']	 				=	$name;     
                     	$params['email']				=	$email;
                     	//$params['phone']	 			=	$phone;   
                     	$params['subject']	 			=	$subject; 
                     	$params['message']				=	$message;
                     	
                     	if(isset($submit))
                     	{
                     		//for mail function
                                  $emailTo ='info@traiac.com';
                			     	$subject = "Traiac Queries";
                			     	$msg = "Name:".$name."\n"."Email:".$email."\n"."Your Queries:".$message;
                			     	mail($emailTo ,$subject,$msg);
                			     	
                     	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Your queries send successfully!"]);
                     	
                     	redirect('Contact/index');	
                     	
                     	}
                     	else {
                			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to send query!"]);
                			redirect('Contact/index');
                		}
                // Paste mail function or whatever else you want to happen here!
               // echo '<br><p>CAPTCHA was completed successfully!</p><br>';
            }
	
	}

        public function quick_mail() {
            
             // Call the function post_captcha
            $res = $this->post_captcha($_POST['g-recaptcha-response']);
        
            if (!$res['success']) {
                // What happens when the CAPTCHA wasn't checked
           //     echo '<p>Make sure you check the security CAPTCHA box.</p><br>';
                 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Make sure you check the security CAPTCHA box."]);
                		redirect($_SERVER['HTTP_REFERER']);	
            } else {
                // If CAPTCHA is successfully completed...
                            
            		$name					= NULL;
                 	$email					= NULL;
                 	$subject		    	= NULL; 
                 	$message		    	= NULL;  
                 	 	
                 	$submit 				= NULL;
                 	
                 	extract($_POST);
                 	$params['name']	 				=	$name;     
                 	$params['email']				=	$email;
                 	//$params['phone']	 			=	$phone;   
                 	$params['subject']	 			=	$subject; 
                 	$params['message']				=	$message;
                 	
                 	if(isset($submit))
                 	{
                 		//for mail function
                            $emailTo = 'info@traiac.com';
            			     	$subject = "Traiac Queries";
            			     	$msg = "Name:".$name."\n"."Email:".$email."\n"."Your Queries:".$message;
            			     	mail($emailTo,$subject,$msg);
            			     	
                 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Your queries send successfully!"]);
                 	
                 	redirect($_SERVER['HTTP_REFERER']);	
                 	
                 	}
            }    	
	}
	



}
